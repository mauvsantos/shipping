import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormComponent } from './form/form.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule, MatSelectModule, MatButtonModule, MatToolbarModule, MatSnackBarModule} from '@angular/material';
import { ThreeDigitDecimaNumberDirective } from './helpers/decimals.directive';
import { NgxCurrencyModule } from 'ngx-currency';

import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material';

import { mockBackendProvider } from './helpers/mock.backend';
import { ExchangeService, RequestShippingService } from './services';

export const customCurrencyMaskConfig = {
	align: 'right',
	allowNegative: true,
	allowZero: true,
	decimal: ',',
	precision: 2,
	prefix: '',
	suffix: '',
	thousands: '.',
	nullable: true
};

@NgModule({
	declarations: [
		AppComponent,
		ThreeDigitDecimaNumberDirective,
		FormComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		FlexLayoutModule,
		MatInputModule,
		MatSelectModule,
		MatButtonModule,
		MatToolbarModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		NgxCurrencyModule.forRoot(customCurrencyMaskConfig)
	],
	providers: [ ExchangeService, RequestShippingService, mockBackendProvider ],
	bootstrap: [ AppComponent ]
})
export class AppModule { }
