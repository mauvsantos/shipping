/* Abstracting the PackageInitializer info */
import { Validators } from '@angular/forms';

export class PackageInitializer {
	// uniqueId: number;
	name = [];
	weight = [];
	value = [];
	currency: string;
	euroValue: number;

	constructor(name: string = '', weight: string = '', value: string = '', currency: string = 'EUR', euroValue: number = 0) {
		// This is not obviously unique for a scalable application
		// But it is unique in this scope of work.
		// And is just used here to keep track and update the package
		// this.uniqueId = new Date().getTime();

		this.name = [ name ,
			[
				Validators.required,
				Validators.maxLength(32)
			]
		];
		this.weight = [ weight ,
			[
				Validators.required,
				Validators.min(0),
				Validators.max(10)
			]
		];
		this.value = [ value ,
			[
				Validators.required,
				Validators.min(0.01)
			]
		];

		const allowedCurrencies = ['EUR', 'USD', 'GBP'];
		const approvedCurrency = allowedCurrencies.includes(currency) ? currency : 'EUR';
		this.currency = approvedCurrency;

		this.euroValue = euroValue;
	}
}

export class Package {
	name: string;
	weight: number;
	value: number;
	currency: string;
	euroValue: number;
}

export class CleanPackage {
	name: string;
	weight: number;
	value: number;

	constructor(name: string, weight: number, value: number) {
		this.name = name;
		this.weight = Number(weight);
		this.value = Number(value);
	}
}
