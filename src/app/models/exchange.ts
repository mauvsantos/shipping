class ExchangeObject {
	amount: number;
	currency: string;
}

export class Exchange {
	from: ExchangeObject;
	to: ExchangeObject;
}
