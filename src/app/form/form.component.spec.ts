import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {FormArray, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FormComponent } from './form.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule, MatSelectModule, MatButtonModule, MatProgressSpinnerModule, MatSnackBarModule} from '@angular/material';
import { PackageInitializer } from '../models/packageInitializer';
import {HttpClientModule} from '@angular/common/http';


describe('FormComponent', () => {
	let component: FormComponent;
	let fixture: ComponentFixture<FormComponent>;
	const initialPackageQuantity = 1;
	const maxPackageQuantity = 5;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ FormComponent ],
			imports: [
				FormsModule,
				HttpClientModule,
				ReactiveFormsModule,
				BrowserAnimationsModule,
				MatInputModule,
				MatSelectModule,
				MatButtonModule,
				MatSnackBarModule,
				MatProgressSpinnerModule
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(FormComponent);
		component = fixture.componentInstance;
		component.ngOnInit()
		fixture.detectChanges();
	});

	it('should create the component', () => {
		expect(component).toBeTruthy();
	});

	it('should have invalid form when empty', () => {
		expect(component.shippingForm.valid).toBeFalsy();
	});

	it('should have 1 package as initial value', () => {
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity);
	});

	it('should be able to add 1 package', () => {
		// Check if allPackages are on initial value
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity);

		// Add package and check if is not empty anymore
		component.addPackage();
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity + 1);
	});

	it('should not be able to add more than 5 allPackages', () => {
		// Check if allPackages are on initial value
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity);

		// Add package and check if is not empty anymore
		component.addPackage();
		component.addPackage();
		component.addPackage();
		component.addPackage();
		component.addPackage();
		component.addPackage();
		expect(component.allPackages.controls.length).toBe(maxPackageQuantity);
	});

	it('should be able to delete allPackages', () => {
		// Check if allPackages are on initial value
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity);

		// Add package and check if is not empty anymore
		component.addPackage();
		expect(component.allPackages.controls.length).toBe(2);

		// Now try to delete it and check if is on initial value again
		component.deletePackage(0);
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity);
	});

	it('should not be able to delete the last package', () => {
		// Check if allPackages are on initial value
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity);

		// Now try to delete it and it didn't
		component.deletePackage(0);
		expect(component.allPackages.controls.length).toBe(initialPackageQuantity);
	});

	it('should not be able to submit an invalid form', () => {
		// Check if allPackages are on initial value
		// Check if allPackages are on initial value
		// expect(component.allPackages.controls.length).toBe(initialPackageQuantity);
		//
		// // Add package and check if is not empty anymore
		// component.addPackage();
	});
});
