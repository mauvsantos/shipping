import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {CleanPackage, Package, PackageInitializer} from '../models/packageInitializer';
import {ExchangeService, RequestShippingService} from '../services';
import {first, map} from 'rxjs/operators';
import {Exchange} from '../models/exchange';
import {roundNumbers} from '../helpers/round';
import {MatSnackBar} from '@angular/material';

@Component({
	selector: 'app-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

	shippingForm: FormGroup;
	formBuilder: FormBuilder;
	private formSubmitted: boolean;
	public convertingCurrency: boolean;

	/* Custom "Total" Validator */
	static totalWeightValidator(group: FormGroup) {
		let sum = 0;
		const packageList = group.get('packages') as FormArray;

		for (const item of packageList.controls) {
			sum += Number(item.get('weight').value);
		}

		return sum > 25 ? { notValid: true } : null;
	}

	constructor(
		public fb: FormBuilder,
		private exchangeService: ExchangeService,
		private requestShipping: RequestShippingService,
		private snackBar: MatSnackBar
	) {
		this.formSubmitted = false;
		this.formBuilder = fb;
		this.convertingCurrency = false;
	}

	ngOnInit() {
		this.createForm();
	}

	createForm() {
		this.shippingForm = this.fb.group({
			totalPackages: [{ value: '1', disabled: true }],
			totalWeight: new FormControl({ value: '0', disabled: true }),
			totalValue: new FormControl({ value: '0', disabled: true }),
			packages: this.fb.array([]),
		}, { validator: FormComponent.totalWeightValidator });

		// Add the first initial package
		this.addPackage();
	}

	/* Getters/Setters */
	get allPackages() {
		return this.shippingForm.get('packages') as FormArray;
	}

	get allPackagesList() {
		return this.allPackages.controls;
	}

	get packageQuantity() {
		return this.allPackages.controls.length;
	}

	set packageQuantity(valueToSet: number) {
		this.shippingForm.patchValue({ totalPackages: this.packageQuantity });
	}

	get totalValue() {
		return this.shippingForm.get('totalValue').value as number;
	}

	set totalValue(valueToSet: number){
		this.shippingForm.patchValue({ totalValue: roundNumbers(valueToSet, 2) });
	}

	get totalWeight() {
		return this.shippingForm.get('totalWeight').value as number;
	}

	set totalWeight(valueToSet: number) {
		this.shippingForm.patchValue({ totalWeight: roundNumbers(valueToSet, 4) });
	}

	/*
	* Totals
	*/
	calculateTotalWeight(items = this.allPackagesList) {
		let weight = 0;
		items.forEach(item => {
			weight += Number(item.get('weight').value);
		});

		this.totalWeight = weight;
	}

	calculateTotalValue(items = this.allPackagesList, shouldExchange: boolean = true) {
		let value = 0;
		items.forEach(item => {
			const itemCurrency = item.get('currency').value;
			const itemValue = item.get('value').value;
			const itemEuroValue = item.get('euroValue').value;

			if (itemCurrency === 'EUR') {
				// If we already have the EUR value, why exchange
				value += Number(itemValue);
			} else {
				value += Number(itemEuroValue);
			}
		});

		this.totalValue = value;
	}

	calculatePackageNumber() {
		this.packageQuantity = 1;
	}

	/* Package Handling */
	getPackageAtPosition(index: number): AbstractControl {
		return this.allPackages.at(index);
	}

	addPackage() {
		if (this.packageQuantity < 5) {
			const index = this.packageQuantity;
			const packInitializer = new PackageInitializer();
			const pack = this.fb.group(packInitializer);

			pack.get('value').valueChanges.subscribe(value => {
				const fullPackage = this.getPackageAtPosition(index);
				const currency = fullPackage.get('currency').value;
				if (currency !== 'EUR') {
					this.exchangeCurrency(index, fullPackage.get('value').value, currency);
				} else {
					this.updateEuroPackageWithId(index, value);
					this.calculateTotalValue();
				}
			});

			pack.get('currency').valueChanges.subscribe(currency => {
				const fullPackage = this.getPackageAtPosition(index);
				if (currency !== 'EUR') {
					this.exchangeCurrency(index, fullPackage.get('value').value, currency);
				} else {
					this.convertingCurrency = false;
					this.calculateTotalValue();
				}
			});

			pack.get('weight').valueChanges.subscribe(() => {
				this.calculateTotalWeight();
			});

			this.allPackages.push(pack);
			this.calculatePackageNumber();
		}
	}

	deletePackage(i) {
		if (this.packageQuantity > 1) {
			this.allPackages.removeAt(i);
			this.calculateTotalValue();
			this.calculatePackageNumber();
		}
	}

	updateEuroPackageWithId(index: number, euroValue: number) {
		const pack = this.getPackageAtPosition(index);
		if (pack !== null) {
			pack.patchValue({ euroValue });
		}
	}

	/*
	* Currency Handling
	*/
	exchangeCurrency(uniqueId: number, amount: number = 0, currency: string = 'EUR') {
		this.convertingCurrency = true;
		const success = (exchange: Exchange): void => {
			// Just Making sure they are numbers...
			const exchangedValue = Number(exchange.to.amount);

			this.updateEuroPackageWithId(uniqueId, exchangedValue);
			this.calculateTotalValue();
			this.convertingCurrency = false;
		};

		const exchangeAmount = (amount) ? amount.toFixed(2) : '0';
		this.exchangeService.exchangeMoney(exchangeAmount, currency).pipe(first()).subscribe(success);
	}

	/*
	* Form Handling
	*/
	sendShipment() {
		if (this.shippingForm) {
			this.formSubmitted = true;
			if (this.shippingForm.invalid) {
				return;
			}

			const { value } = this.shippingForm;

			value.packages = value.packages.map(item => {
				const newItem = new CleanPackage(item.name, item.weight, item.euroValue);
				return newItem;
			});

			this.requestShipping.submitRequest(value)
				.pipe(first())
				.subscribe(
					data => {
						console.log('Success? :)');
						console.log(data);

						this.snackBar.open('Success!', 'Close', {
							duration: 20000,
						});
						this.createForm();
						this.calculateTotalValue();
					},
					response => {

						this.snackBar.open(response.error.message, 'Close', {
							duration: 20000,
						});
						console.log('Error? :(');
						console.log(response);
					});
		}
	}
}
