import {async, TestBed} from '@angular/core/testing';

import {RequestShippingService} from './request-shipping.service';
import {FormComponent} from '../form/form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule} from '@angular/material';

describe('RequestShippingService', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [FormComponent],
			imports: [
				FormsModule,
				HttpClientModule,
				ReactiveFormsModule,
				BrowserAnimationsModule,
				MatInputModule,
				MatSelectModule,
				MatButtonModule,
				MatProgressSpinnerModule
			]
		})
			.compileComponents();
	}));

	it('should be created', () => {
		const service: RequestShippingService = TestBed.get(RequestShippingService);
		expect(service).toBeTruthy();
	});
});
