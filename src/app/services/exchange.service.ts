import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Exchange } from '../models/exchange';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class ExchangeService {

	apiUrl: string;

	constructor(private http: HttpClient) {
		this.apiUrl = environment.apiUrl;
	}

	exchangeMoney(amount: string, currency: string): Observable<Exchange> {
		return this.http.get<Exchange>(`${this.apiUrl}exchange/`, {
			params: { currency, amount }
		});
	}

}
