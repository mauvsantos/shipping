import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class RequestShippingService {

	apiUrl: string;

	constructor(private http: HttpClient) {
		this.apiUrl = environment.apiUrl;
	}

	submitRequest(packageSubmit: any) {
		return this.http.post(`${this.apiUrl}/request-shipping`, packageSubmit);
	}
}
