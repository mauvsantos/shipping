/*
* Much better and safer method to round numbers than toFixed(x) because of multiple reasons
* Note: Not my code, just tweaked...
* -----
* Reference:
* https://stackoverflow.com/a/21323330/1026520
* */
export function roundNumbers(value, exp) {
	if (typeof exp === 'undefined' || +exp === 0) {
		return Math.round(value);
	}

	value = +value;
	exp = +exp;

	if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)){
		return NaN;
	}

	// Shift
	value = value.toString().split('e');
	value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

	// Shift back
	value = value.toString().split('e');
	return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
};
