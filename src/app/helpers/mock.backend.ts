import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { mockExchangeRate } from './mock.data';
import { Exchange } from '../models/exchange';

@Injectable()
export class MockBackendInterceptor implements HttpInterceptor {

	constructor() { }

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const exchangeRate = mockExchangeRate;

		// wrap in delayed observable to simulate server api call
		return of(null).pipe(mergeMap(() => {

			// I am leaving these consoles... it's fancy for debugging :)
			console.groupCollapsed('[ HTTP INTERCEPTOR ]');
			console.log('Tried to intercept: [', request.method + ': ' + request.url, ']');

			// calculate EUR from WHATEVER comes
			if (request.url.match(/\/exchange\//) && request.method === 'GET') {
				console.log('SUCCESS', request.url);

				// find if currency is in the list (in our case always is, but for safety)
				const requestedCurrency = request.params.get('currency');
				const requestedValue = Number(request.params.get('amount'));
				const currencyRate = exchangeRate.rates[requestedCurrency];

				console.log('Exchange rates used:');
				console.table(exchangeRate.rates);

				if (currencyRate != null && !isNaN(requestedValue)) {
					// if everything is ok, return a 200 OK with the rate calculated already in EUR
					const body: Exchange = {
						from: {
							amount: requestedValue,
							currency: requestedCurrency
						},
						to: {
							// This "exchanging" should be written in a different place, but since this is a mock service...
							amount: Number((requestedValue / currencyRate).toFixed(2)),
							currency: 'EUR'
						}
					};

					console.groupEnd();
					return of(new HttpResponse({ status: 200, body }));
				} else {
					console.log('ERROR: Intercepted url but some error ocurred ', request.url);
					console.groupEnd();
					// else return 400 bad request
					return throwError({ error: { message: 'No exchange rate found for this currency or invalid amount' } });
				}
			}


			if (request.url.endsWith('/request-shipping') && request.method === 'POST') {
				console.log('SUCCESS', request.url);

				if (request.body) {
					// if login details are valid return 200 OK with user details and fake jwt token
					const body = {
						status: 'success',
						message: 'Shipping request create with success!',
						shipping: request.body
					};

					console.log('Request made:');
					console.table(JSON.stringify(request.body));
					console.groupEnd();

					return of(new HttpResponse({ status: 200, body }));
				} else {
					console.log('ERROR: Intercepted url but some error ocurred ', request.url);
					console.groupEnd();
					// else return 400 bad request
					return throwError({ error: { message: 'Something is definitely wrong with this request...' } });
				}
			}

			// pass through any requests not handled above
			return next.handle(request);

		}))

		// call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
			.pipe(materialize())
			.pipe(delay(500))
			.pipe(dematerialize());
	}
}

export let mockBackendProvider = {
	// use fake backend in place of Http service for backend-less development
	provide: HTTP_INTERCEPTORS,
	useClass: MockBackendInterceptor,
	multi: true
};
