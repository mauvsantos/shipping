# ShippingApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Starting up the project

To start up the project you just need to install the required packages & run using the Angular CLI:
```
$ npm install
$ ng serve
```

## Checking tests coverage
Code coverage is not 100% but that would be an improvement
- It is not at 100% due to lack of time

```
$ ng test --code-coverage
$ http-server -c-1 -o -p 9875 ./coverage
```
